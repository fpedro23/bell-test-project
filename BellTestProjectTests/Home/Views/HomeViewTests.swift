import XCTest
import CoreLocation

@testable import BellTestProject

class HomeViewTests: XCTestCase {

    var dispatcher: Dispatcher<HomeAction>!
    var subject: HomeView!
    var processor: MockProcessor<HomeAction>!

    override func setUp() {
        processor = MockProcessor()
        dispatcher = Dispatcher(processor)

        subject = HomeView()
        subject.dispatcher = dispatcher

        let viewController = UIViewController()
        viewController.view.addConstrained(subview: subject)
        UIApplication.shared.windows[0].rootViewController = viewController
    }

    func testPresent() {
        subject.present(.default)
        XCTAssertEqual(subject.radiusSelectionView.slider.value, 5000.0)
        XCTAssertEqual(subject.radiusSelectionView.label.text, "5 km")
    }

    func testPresentWithAnnotations() {
        subject.present(.default)
        XCTAssertEqual(subject.mapView.annotations.count, 100)

        subject.present(.with25Tweets)
        XCTAssertEqual(subject.mapView.annotations.count, 25)
    }

    func testApplyEffects() {
        let center = CLLocationCoordinate2D(latitude: 180, longitude: 180)
        subject.apply(.showRadiusCircle(location: center, radius: 5))
        XCTAssertEqual(subject.mapView.overlays.count, 1)
    }

    func testLocationButton() {
        subject.locationButton.sendActions(for: .primaryActionTriggered)
        XCTAssertEqual(processor.dispatchedActions, [.didTapUserLocation])
    }

    func testConfigureAnnotationView() {
        let view = TweetAnnotationView(annotation: TweetAnnotation(tweet: .default), reuseIdentifier: "id")
        subject.mapView(subject.mapView, didSelect: view)
        XCTAssertEqual(processor.dispatchedActions, [.configure(view)])
    }
}
