import XCTest
import CoreLocation

@testable import BellTestProject

class HomeProcessorTests: XCTestCase {

    var client: TestClient!
    var subject: HomeProcessor!
    var navigation: MockNavigation!
    var presenter: MockPresenter<HomeState, HomeEffect>!
    var state: HomeState!
    var locationManager: MockLocationManager!
    var mockStore: MockSessionStore!

    enum HomeErrorTest: Error {
        case notFound
    }

    override func setUp() {
        navigation = MockNavigation()
        client = TestClient()
        locationManager = MockLocationManager()
        mockStore = MockSessionStore()
        presenter = MockPresenter()
        let services = ServiceContainer(locationService: locationManager,
                         twitterAPI: client,
                         sessionStore: mockStore)
        subject = HomeProcessor(navigation: navigation,
                                presenter: presenter,
                                services: services,
                                state: HomeState())
    }

    func testDidReceiveDidTapUserLocation() {
        locationManager.didRequestInUseAuthorization = false
        subject.receive(.didTapUserLocation)
        XCTAssertTrue(locationManager.didRequestInUseAuthorization!)
    }

    func testDidReceiveDidTapUserLocationCentersUser() {
        locationManager.didRequestInUseAuthorization = false
        let location = CLLocation(latitude: 10, longitude: 10)
        locationManager.location = location
        subject.receive(.didTapUserLocation)
        XCTAssertEqual(presenter.applied, [.centerMap(location: location.coordinate, radius: 5000.0)])
    }

    func testShowTweetDetails() {
        subject.receive(.showTweetDetails(.default))
        XCTAssertEqual(navigation.routes, [.showTweetDetails(.default)])
    }

    func testRadiusDidChange() {
        locationManager.location = CLLocation(latitude: 10, longitude: 10)
        subject.receive(.radiusDidChange(radius: 25))
        XCTAssertEqual(subject.state.radiusSearch, 25)
        XCTAssertEqual(presenter.applied, [
            .showRadiusCircle(location: locationManager.location!.coordinate, radius: 25),
            .centerMap(location: locationManager.location!.coordinate, radius: 25)
        ])
    }

    func testSearchTweetsLocationEnabled() {
        locationManager.authorizationStatusToReturn = .authorizedWhenInUse
        locationManager.location = CLLocation(latitude: 10, longitude: 10)
        subject.receive(.searchTweets("✅"))
        XCTAssertEqual(presenter.applied, [
            .centerMap(location: locationManager.location!.coordinate, radius: 5000),
            .showRadiusCircle(location: locationManager.location!.coordinate, radius: 5000)
        ])
        XCTAssertEqual(subject.state.tweets.count, 100)
    }

    func testSearchTweetsLocationNotEnabled() {
        locationManager.authorizationStatusToReturn = .notDetermined
        subject.receive(.searchTweets("❗️"))
        XCTAssertEqual(navigation.alertsPresented[0].title, "Location services")
        XCTAssertEqual(navigation.alertsPresented[0].message, "Location services are needed for this app to function, enable location services and try again")
        XCTAssertEqual(navigation.alertsPresented[0].alertActions.count, 2)
        navigation.alertsPresented[0].alertActions[1].handler?(AlertAction(title: "", style: .default))
        XCTAssertTrue(locationManager.didRequestInUseAuthorization!)
    }

    func testSearchTweetsLocationEnabledError() {
        locationManager.authorizationStatusToReturn = .authorizedWhenInUse
        locationManager.location = CLLocation(latitude: 10, longitude: 10)
        client.errorToReturn = HomeErrorTest.notFound
        subject.receive(.searchTweets("❗️"))
        XCTAssertEqual(presenter.applied, [
            .centerMap(location: locationManager.location!.coordinate, radius: 5000),
            .showRadiusCircle(location: locationManager.location!.coordinate, radius: 5000)
        ])
        XCTAssertEqual(subject.state.tweets.count, 0)
        XCTAssertEqual(navigation.alertsPresented[0].title, "Error")
        XCTAssertEqual(navigation.alertsPresented[0].message, "The operation couldn’t be completed. (BellTestProjectTests.HomeProcessorTests.HomeErrorTest error 0.)")
        XCTAssertEqual(navigation.alertsPresented[0].alertActions.count, 1)
    }

    func testConfigureTweetView() {
        let tweetAnnotation = TweetAnnotation(tweet: .default)!
        let view = TweetAnnotationView(annotation: tweetAnnotation, reuseIdentifier: "identifier")
        subject.receive(.configure(view))
        XCTAssertEqual(view.detailCalloutAccessoryView, view.stackView)
    }

    func testConfigureTweetViewError() {
        client.errorToReturn = HomeErrorTest.notFound
        let tweetAnnotation = TweetAnnotation(tweet: .default)!
        let view = TweetAnnotationView(annotation: tweetAnnotation, reuseIdentifier: "identifier")
        subject.receive(.configure(view))
        XCTAssertNil(view.detailCalloutAccessoryView)
        XCTAssertEqual(navigation.alertsPresented[0].title, "Error")
        XCTAssertEqual(navigation.alertsPresented[0].message, "The operation couldn’t be completed. (BellTestProjectTests.HomeProcessorTests.HomeErrorTest error 0.)")
        XCTAssertEqual(navigation.alertsPresented[0].alertActions.count, 1)
    }

    func testStateShowsMostRecent100Tweets() {
        subject.state.didReceiveSearchResults(results: TweetSearchResults(tweets: (1...100).map { Tweet(identifier: $0 )},
                                                                          next: "next"))
        XCTAssertEqual(
            (1...100).map { $0 },
            subject.state.mapAnnotations.map { $0.tweet.identifier }
        )

        subject.state.didReceiveSearchResults(results: TweetSearchResults(tweets: (101...200).map { Tweet(identifier: $0 )},
        next: "next"))
        XCTAssertEqual(
            (101...200).map { $0 },
            subject.state.mapAnnotations.map { $0.tweet.identifier }
        )

        XCTAssertEqual(subject.state.tweets.count, 200)
        XCTAssertEqual(subject.state.mapAnnotations.count, 100)

        let identifierSet = Set(subject.state.mapAnnotations.map { $0.tweet.identifier })
        let nonContainedSet = Set<Int64>((1...100).map { $0 })
        XCTAssertEqual(identifierSet.intersection(nonContainedSet).count, 0)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

}
