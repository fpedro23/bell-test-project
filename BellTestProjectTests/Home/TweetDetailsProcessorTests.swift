import XCTest
import CoreLocation
import TwitterKit

@testable import BellTestProject

class TweetDetailsProcessorTests: XCTestCase {

    var client: TestClient!
    var subject: TweetDetailsProcessor!
    var navigation: MockNavigation!
    var presenter: MockPresenter<TweetDetailsState, Void>!
    var state: TweetDetailsState!
    var locationManager: MockLocationManager!
    var mockStore: MockSessionStore!

    enum ErrorTest: Error {
        case notFound
    }

    override func setUp() {
        navigation = MockNavigation()
        client = TestClient()
        locationManager = MockLocationManager()
        mockStore = MockSessionStore()
        presenter = MockPresenter()
        let services = ServiceContainer(locationService: locationManager,
                         twitterAPI: client,
                         sessionStore: mockStore)
        subject = TweetDetailsProcessor(navigation: navigation,
                                presenter: presenter,
                                services: services,
                                state: TweetDetailsState(tweet: .default))
    }

    func testDidReceiveLoadTweet() {
        XCTAssertNil(subject.state.tweet)
        subject.receive(.loadTweet)
        XCTAssertNotNil(subject.state.tweet)
    }

    func testDidReceiveLoadTweetError() {
        client.errorToReturn = ErrorTest.notFound
        XCTAssertNil(subject.state.tweet)
        subject.receive(.loadTweet)
        XCTAssertNil(subject.state.tweet)
        XCTAssertEqual(navigation.alertsPresented[0].title, "Error")
        XCTAssertEqual(navigation.alertsPresented[0].message, "The operation couldn’t be completed. (BellTestProjectTests.TweetDetailsProcessorTests.ErrorTest error 0.)")
        XCTAssertEqual(navigation.alertsPresented[0].alertActions.count, 1)
    }

    func testRetweetWithNoUser() {
        mockStore.currentSession = nil
        subject.receive(.retweet)
        XCTAssertTrue(client.loginProcessStarted)
    }

    func testRetweetWithUser() {
        mockStore.currentSession = TWTRSession(authToken: "🔥",
                                               authTokenSecret: "👀",
                                               userName: "⭐️",
                                               userID: "🐢")
        subject.receive(.retweet)
        XCTAssertFalse(client.loginProcessStarted)
        XCTAssertEqual(navigation.alertsPresented[0].title, "Success")
        XCTAssertEqual(navigation.alertsPresented[0].message, "The tweet has been retweeted succesfully.")
        XCTAssertEqual(navigation.alertsPresented[0].alertActions.count, 1)
    }

    func testRetweetWithUserError() {
        client.errorToReturn = ErrorTest.notFound
        mockStore.currentSession = TWTRSession(authToken: "🔥",
                                               authTokenSecret: "👀",
                                               userName: "⭐️",
                                               userID: "🐢")
        subject.receive(.retweet)
        XCTAssertFalse(client.loginProcessStarted)
        XCTAssertEqual(navigation.alertsPresented[0].title, "Error")
        XCTAssertEqual(navigation.alertsPresented[0].message, "The operation couldn’t be completed. (BellTestProjectTests.TweetDetailsProcessorTests.ErrorTest error 0.)")
        XCTAssertEqual(navigation.alertsPresented[0].alertActions.count, 1)
    }
}
