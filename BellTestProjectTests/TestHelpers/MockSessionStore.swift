import Foundation
import TwitterKit

@testable import BellTestProject

class MockSessionStore: SessionStore {

    var currentSession: TWTRAuthSession?

    func didLoadSession(session: TWTRAuthSession) {
        currentSession = session
    }
}
