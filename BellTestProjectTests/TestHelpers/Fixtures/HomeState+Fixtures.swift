import Foundation

@testable import BellTestProject

extension HomeState {
    static var `default`: HomeState {
        var state = HomeState()
        let results = TweetSearchResults(tweets: (1...100).map { Tweet(identifier: $0 )}, next: "next")
        state.didReceiveSearchResults(results: results)
        return state
    }

    static var with25Tweets: HomeState {
        var state = HomeState()
        let results = TweetSearchResults(tweets: (1...25).map { Tweet(identifier: $0 )}, next: "next")
        state.didReceiveSearchResults(results: results)
        return state
    }
}
