@testable import BellTestProject

extension Tweet {
    static let `default` = Tweet(identifier: 1234,
                                 text: "🦜",
                                 user: .default,
                                 coordinates: Coordinates(coordinates: [0, 0]),
                                 place: nil)
}

extension User {
    static let `default` = User(id: 1, name: "🐢")
}

extension Tweet {
    init(identifier: Int64) {
        self.init(identifier: identifier,
              text: "🦜",
              user: .default,
              coordinates: Coordinates(coordinates: [0, 0]),
              place: nil)
    }
}
