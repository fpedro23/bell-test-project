import Foundation

@testable import BellTestProject

class MockProcessor<T>: Processor {
    var dispatchedActions: [T] = []

    func receive(_ action: T) {
        dispatchedActions.append(action)
    }
}
