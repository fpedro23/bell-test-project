import Foundation
import UIKit

@testable import BellTestProject

class MockNavigation: Navigation, AlertPresentable {

    var alertsPresented: [Alert] = []
    var contexts: [AnyObject?] = []
    var navigateToRouteCalls: [Route?] = []
    var rootViewController: UIViewController = UIViewController()
    var routes: [Route] = []

    func navigate(to route: Route, context: AnyObject?) {
        routes.append(route)
        contexts.append(context)
    }

    func present(_ alert: Alert) {
        alertsPresented.append(alert)
    }
}
