import CoreLocation
import Foundation

@testable import BellTestProject

public class MockLocationManager: LocationManager {
    public var activityType: CLActivityType = .other
    public var authorizationStatusToReturn = CLAuthorizationStatus.authorizedWhenInUse
    public var desiredAccuracy: CLLocationAccuracy = kCLLocationAccuracyBest
    public var didRequestAlwaysAuthorization: Bool?
    public var didRequestInUseAuthorization: Bool?
    public var didRequestLocation: Bool?
    public var distanceFilter: CLLocationDistance = kCLDistanceFilterNone
    public var isMonitoringSignificantLocationChanges = false
    public var isUpdatingLocation = false
    public var locationServicesEnabledToReturn = true
    public var location: CLLocation?
    public var locationToReturn: CLLocation? = CLLocation(latitude: 44.9778, longitude: -93.2650)
    public var errorToReturn: Error?
    public weak var delegate: CLLocationManagerDelegate?
    private var manager = CLLocationManager()
    public var monitoredRegions = Set<CLRegion>()
    public var regionStatesRequests = [CLRegion]()

    public init() {}

    public func locationServicesEnabled() -> Bool {
        return locationServicesEnabledToReturn
    }

    public func authorizationStatus() -> CLAuthorizationStatus {
        return authorizationStatusToReturn
    }

    public func requestAlwaysAuthorization() {
        didRequestAlwaysAuthorization = true
        delegate?.locationManager!(CLLocationManager(), didChangeAuthorization: authorizationStatus())
    }

    public func requestWhenInUseAuthorization() {
        didRequestInUseAuthorization = true
        delegate?.locationManager!(CLLocationManager(), didChangeAuthorization: authorizationStatus())
    }

    public func requestLocation() {
        didRequestLocation = true

        if let error = errorToReturn {
            delegate?.locationManager!(CLLocationManager(), didFailWithError: error)
        } else if let location = locationToReturn {
            delegate?.locationManager!(CLLocationManager(), didUpdateLocations: [location])
        }
    }

    public func startUpdatingLocation() {
        isUpdatingLocation = true
    }

    public func stopUpdatingLocation() {
        isUpdatingLocation = false
    }

    public func startMonitoringSignificantLocationChanges() {
        isMonitoringSignificantLocationChanges = true
    }

    public func stopMonitoringSignificantLocationChanges() {
        isMonitoringSignificantLocationChanges = false
    }

    public func startMonitoring(for region: CLRegion) {
        monitoredRegions.insert(region)
    }

    public func stopMonitoring(for region: CLRegion) {
        monitoredRegions.remove(region)
    }

    public func requestState(for region: CLRegion) {
        regionStatesRequests.append(region)
    }
}

extension MockLocationManager {
    public func didEnterRegion(_ region: CLRegion) {
        delegate?.locationManager?(manager, didEnterRegion: region)
    }

    public func didExitRegion(_ region: CLRegion) {
        delegate?.locationManager?(manager, didExitRegion: region)
    }

    public func didDetermineState(_ state: CLRegionState, for region: CLRegion) {
        delegate?.locationManager?(manager, didDetermineState: state, for: region)
    }

    public func didStartMonitoring(for region: CLRegion) {
        delegate?.locationManager?(manager, didStartMonitoringFor: region)
    }

    public func didUpdateLocations(_ locations: [CLLocation]) {
        delegate?.locationManager?(manager, didUpdateLocations: locations)
    }
}
