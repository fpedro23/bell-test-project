import UIKit

@testable import BellTestProject

class MockPresenter<StateType, EffectType> {
    var applied = [EffectType]()
    var presented = [StateType]()
    var converted = [UIView]()

    func apply(_ effect: EffectType) {
        applied.append(effect)
    }

    func present(_ state: StateType) {
        presented.append(state)
    }
}

extension MockPresenter: HomePresenter where StateType == HomeState, EffectType == HomeEffect {}
extension MockPresenter: TweetDetailsPresenter where StateType == TweetDetailsState, EffectType == Void {}
