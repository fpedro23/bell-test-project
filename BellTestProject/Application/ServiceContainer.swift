/// Protocol for an object that provides a `LocationService`.
///
protocol HasLocationService {

    /// The `LocationService` used to get the user's current location.
    var locationService: LocationManager { get }
}

protocol HasTwitterService {

    var twitterAPI: TwitterAPI { get }
}

protocol HasSessionStore {
    var sessionStore: SessionStore { get }
}

/// Typealias for the services provided by the `ServiceContainer`.
///
private typealias Services = HasLocationService & HasTwitterService & HasSessionStore

struct ServiceContainer: Services {

    let locationService: LocationManager

    let twitterAPI: TwitterAPI

    let sessionStore: SessionStore

}
