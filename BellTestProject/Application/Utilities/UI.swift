import Foundation
import UIKit

// swiftlint:disable type_name

enum UI {

    static var animated = true

    static func duration(_ duration: TimeInterval) -> TimeInterval {
        return animated ? duration : 0.0
    }

    static func after(_ after: TimeInterval) -> DispatchTime {
        return animated ? .now() + after : .now()
    }
}
