import CoreLocation
import Foundation

/// Protocol for an object that can fetch the devices location. Used for testing `CLLocationManager`.
///
public protocol LocationManager: class {

    // MARK: Properties

    /// The type of user activity associated with the location updates.
    var activityType: CLActivityType { get set }

    /// A `CLLocationManagerDelegate` delegate object to receive update events.
    var delegate: CLLocationManagerDelegate? { get set }

    /// The desired accuracy of the location data.
    var desiredAccuracy: CLLocationAccuracy { get set }

    /// The minimum distance (measured in meters) a device must move horizontally before an update event is generated.
    var distanceFilter: CLLocationDistance { get set }

    /// The most recently retrieved user location, if available.
    var location: CLLocation? { get }

    /// The list of regions currently being monitored by the manager for enter and exit events.
    var monitoredRegions: Set<CLRegion> { get }

    // MARK: Service Availability

    /// Returns the app's authorization status for using location services.
    ///
    /// - Returns: The app's authorization status for using location services.
    ///
    func authorizationStatus() -> CLAuthorizationStatus

    /// Returns true if location service are enabled on the device, or false otherwise.
    ///
    /// - Returns: True if location services are enabled, false otherwise.
    ///
    func locationServicesEnabled() -> Bool

    // MARK: Requesting Authorization

    /// Requests authorization to use location services whenever the app is running.
    ///
    func requestAlwaysAuthorization()

    /// Requests authorization to use location services while the app is in the foreground.
    ///
    func requestWhenInUseAuthorization()

    // MARK: Standard Location Updates

    /// Requests a one-time delivery of the user's current location.
    ///
    func requestLocation()

    /// Starts the generation of updates that report the user’s current location.
    ///
    func startUpdatingLocation()

    /// Stops the generation of location updates.
    ///
    func stopUpdatingLocation()

    // MARK: Significant Location Updates

    /// Starts the generation of updates based on significant location changes.
    ///
    func startMonitoringSignificantLocationChanges()

    /// Stops the delivery of location events based on significant location changes.
    ///
    func stopMonitoringSignificantLocationChanges()

    // MARK: Region Monitoring

    /// Retrieves the state of a region asynchronously.
    ///
    /// - Parameter region: The region to retrieve the state of.
    ///
    func requestState(for region: CLRegion)

    /// Starts monitoring the specified region.
    ///
    /// - Parameter for: The region that defines the boundary to monitor.
    ///
    func startMonitoring(for: CLRegion)

    /// Stops monitoring the specified region.
    ///
    /// - Parameter for: The region currently being monitored that shouldn't be monitored any longer.
    ///
    func stopMonitoring(for: CLRegion)
}

extension CLLocationManager: LocationManager {
    public func authorizationStatus() -> CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }

    public func locationServicesEnabled() -> Bool {
        return CLLocationManager.locationServicesEnabled()
    }
}
