import Foundation
// MARK: Top levels routes

enum Route: Equatable {
    case home
    case showTweetDetails(Tweet)
}

protocol Navigation: class, AlertPresentable {
    func navigate(to route: Route, context: AnyObject?)

    func navigate(to route: Route)
}

extension Navigation {
    func navigate(to route: Route) {
        navigate(to: route, context: nil)
    }
}
