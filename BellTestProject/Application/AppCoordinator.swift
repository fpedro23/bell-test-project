import UIKit

protocol AppCoordinator: Navigation {

}

class DefaultAppCoordinator: NSObject {

    typealias Services = HasLocationService & HasTwitterService & HasSessionStore

    var viewController: UIViewController {
        return navigationController
    }

    let navigationController: UINavigationController = UINavigationController()

    private var homeProcessor: HomeProcessor?

    private var tweetDetailsProcessor: TweetDetailsProcessor?

    private var presentedCoordinator: AnyObject?

    private var services: Services

    // MARK: Initialization

    init(services: Services) {
        self.services = services
        super.init()
    }
}

// MARK: - AppCoordinator

extension DefaultAppCoordinator: AppCoordinator {
    var rootViewController: UIViewController {
        return viewController
    }

    func navigate(to route: Route, context: AnyObject?) {
        switch route {
        case .home:
            showHome()
        case .showTweetDetails(let tweet):
            showTweetDetails(tweet: tweet)
        }
    }

    private func showHome() {
        let homeViewController = HomeViewController()
        homeProcessor = HomeProcessor(navigation: self,
                                      presenter: homeViewController,
                                      services: services,
                                      state: HomeState())
        homeViewController.dispatcher = homeProcessor?.dispatcher
        navigationController.pushViewController(homeViewController, animated: false)
    }

    private func showTweetDetails(tweet: Tweet) {
        let tweetDetailsViewController = TweetDetailsViewController()
        tweetDetailsProcessor = TweetDetailsProcessor(navigation: self,
                                                      presenter: tweetDetailsViewController,
                                                      services: services,
                                                      state: TweetDetailsState(tweet: tweet))
        tweetDetailsViewController.dispatcher = tweetDetailsProcessor?.dispatcher
        navigationController.pushViewController(tweetDetailsViewController, animated: true)
    }
}
