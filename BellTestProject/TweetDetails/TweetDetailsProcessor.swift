import Foundation
import CoreLocation
import TwitterKit

protocol TweetDetailsPresenter: class {
    func present(_ state: TweetDetailsState)
}

class TweetDetailsProcessor {

    typealias Services =  HasTwitterService & HasSessionStore

    private weak var presenter: TweetDetailsPresenter?

    weak var navigation: Navigation?

    var dispatcher: Dispatcher<TweetDetailsAction>?

    var state: TweetDetailsState {
        didSet {
            presenter?.present(state)
        }
    }

    let services: Services

    init(navigation: Navigation,
         presenter: TweetDetailsPresenter,
         services: Services,
         state: TweetDetailsState) {
        self.navigation = navigation
        self.presenter = presenter
        self.services = services
        self.state = state
        self.dispatcher = Dispatcher(self)
    }

    private func loadTweetDetails() {
        services.twitterAPI.loadTweet(identifier: String(state.tweetIdentifier)) { result in
            switch result {
            case .success(let tweet):
                self.state.tweet = tweet
            case .failure(let error):
                let alert = Alert(title: Localizations.Error.title,
                                  message: error.localizedDescription,
                                  preferredStyle: .alert,
                                  alertActions: [.init(title: Localizations.Home.accept, style: .default)])
                self.navigation?.present(alert)
            }
        }
    }

    private func login(completion: @escaping (Result<TWTRSession, Error>) -> Void) {
        services.twitterAPI.logIn(completion: completion)
    }

    private func loginAndRetweet() {
        login { result in
            switch result {
            case .success(let session):
                self.services.sessionStore.didLoadSession(session: session)
                self.retweet()
            case .failure(let error):
                print(error)
            }
        }
    }

    private func retweet() {
        guard let identifier = services.sessionStore.currentSession?.userID else {
            loginAndRetweet()
            return
        }
        services.twitterAPI.retweet(tweetIdentifier: state.tweetIdentifier, userIdentifier: identifier) { result in
            switch result {
            case .success:
                let alert = Alert(title: Localizations.TweetDetails.success,
                                  message: Localizations.TweetDetails.retweetSuccessMessage,
                                  preferredStyle: .alert,
                                  alertActions: [.init(title: Localizations.Home.accept, style: .default)])
                self.navigation?.present(alert)
            case .failure(let error):
                let alert = Alert(title: Localizations.Error.title,
                                  message: error.localizedDescription,
                                  preferredStyle: .alert,
                                  alertActions: [.init(title: Localizations.Home.accept, style: .default)])
                self.navigation?.present(alert)
            }
        }
    }
}

extension TweetDetailsProcessor: Processor {
    func receive(_ action: TweetDetailsAction) {
        switch action {
        case .loadTweet:
            loadTweetDetails()
        case .retweet:
            retweet()
        }
    }
}
