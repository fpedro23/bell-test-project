import UIKit
import TwitterKit

enum TweetDetailsAction {
    case loadTweet
    case retweet
}

class TweetDetailsViewController: UIViewController {

    var dispatcher: Dispatcher<TweetDetailsAction>?

    let scrollView = UIScrollView()

    lazy var tweetView: TWTRTweetView = {
        let view = TWTRTweetView(tweet: nil, style: .regular)
        view.showActionButtons = true
        switch self.traitCollection.userInterfaceStyle {
        case .dark:
            view.theme = .dark
        case .light:
            view.theme = .light
        case .unspecified:
            view.theme = .light
        @unknown default:
            view.theme = .light
        }
        return view
    }()

    lazy var retweetButtonItem: UIBarButtonItem = {
        UIBarButtonItem(image: UIImage(systemName: "arrow.2.squarepath"),
                        style: .plain,
                        target: self,
                        action: #selector(didTapRetweet))
    }()

    override func viewDidLoad() {
        navigationItem.rightBarButtonItem = retweetButtonItem
        view.backgroundColor = tweetView.backgroundColor
        view.addConstrained(subview: scrollView)
        scrollView.addConstrained(subview: tweetView)
        tweetView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dispatcher?.dispatch(.loadTweet)
    }

    @objc func didTapRetweet() {
        dispatcher?.dispatch(.retweet)
    }
}

extension TweetDetailsViewController: TweetDetailsPresenter {
    func present(_ state: TweetDetailsState) {
        self.tweetView.configure(with: state.tweet)
    }
}
