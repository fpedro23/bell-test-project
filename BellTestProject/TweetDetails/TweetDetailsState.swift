import Foundation
import TwitterKit

struct TweetDetailsState: Equatable {
    let tweetIdentifier: Int64

    var tweet: TWTRTweet?
}

extension TweetDetailsState {
    init(tweet: Tweet) {
        tweetIdentifier = tweet.identifier
    }
}
