import Foundation
import CoreLocation
import TwitterKit

extension CLLocationCoordinate2D: Equatable {
    public static func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        lhs.latitude == rhs.latitude &&
        lhs.longitude == rhs.longitude
    }
}
enum HomeEffect: Equatable {
    case centerMap(location: CLLocationCoordinate2D, radius: Double)
    case showRadiusCircle(location: CLLocationCoordinate2D, radius: Double)
}

protocol HomePresenter: class {
    func present(_ state: HomeState)
    func apply(_ effect: HomeEffect)
}

class HomeProcessor: NSObject {

    typealias Services = HasLocationService & HasTwitterService

    private weak var presenter: HomePresenter?

    weak var navigation: Navigation?

    var dispatcher: Dispatcher<HomeAction>?

    var state: HomeState {
        didSet {
            presenter?.present(state)
        }
    }

    let services: Services

    lazy var timer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true, block: pollTweets)

    init(navigation: Navigation,
         presenter: HomePresenter,
         services: Services,
         state: HomeState) {
        self.navigation = navigation
        self.presenter = presenter
        self.services = services
        self.state = state
        super.init()
        self.dispatcher = Dispatcher(self)
        services.locationService.delegate = self
        self.state.showUserLocation = services.locationService.authorizationStatus() == .authorizedWhenInUse
    }

    private func pollTweets(_ timer: Timer) {
        searchTweets(query: state.currentSearchQuery)
    }

    private func requestCurrentLocation() {
        services.locationService.requestWhenInUseAuthorization()
    }

    private func searchTweets(query: String) {
        let location = services.locationService.location?.coordinate
        if let coordinate = location {
            presenter?.apply(.centerMap(location: coordinate, radius: Double(state.radiusSearch)))
            presenter?.apply(.showRadiusCircle(location: coordinate, radius: Double(state.radiusSearch)))
        }
        services.twitterAPI.searchTweets(query: query, location: location, radius: state.radiusSearch, next: state.nextPage) { result in
            switch result {
            case .success(let results):
                self.state.didReceiveSearchResults(results: results)
                // Commenting out this lines will make the map to poll results in background.
                // Disabled since pagination is only available for premium search.
//                if !self.timer.isValid {
//                    self.timer.fire()
//                }
            case .failure(let error):
                let alert = Alert(title: Localizations.Error.title,
                                  message: error.localizedDescription,
                                  preferredStyle: .alert,
                                  alertActions: [.init(title: Localizations.Home.accept, style: .default)])
                self.navigation?.present(alert)
            }
        }
    }

    private func configure(view: TweetAnnotationView, annotation: TweetAnnotation) {
        services.twitterAPI.loadTweet(identifier: String(annotation.tweet.identifier)) { result in
            switch result {
            case .success(let tweet):
                view.configureView(tweet: tweet)
            case .failure(let error):
                let alert = Alert(title: Localizations.Error.title,
                                  message: error.localizedDescription,
                                  preferredStyle: .alert,
                                  alertActions: [.init(title: Localizations.Home.accept, style: .default)])
                self.navigation?.present(alert)
            }
        }
    }
}

extension HomeProcessor: Processor {
    func receive(_ action: HomeAction) {
        switch action {
        case .didTapUserLocation:
            if let location = services.locationService.location?.coordinate {
                presenter?.apply(.centerMap(location: location, radius: Double(state.radiusSearch)))
                return
            }
            requestCurrentLocation()
        case .searchTweets(let query):
            state.searchQueryDidChange(newQuery: query)
            let status = services.locationService.authorizationStatus()
            if status == .denied || status == .notDetermined {
                let actions = [
                    AlertAction(title: Localizations.Home.cancel, style: .cancel, handler: nil),
                    AlertAction(title: Localizations.Home.authorize, style: .default, handler: { _  in self.requestCurrentLocation() })
                ]
                let alert = Alert(title: Localizations.Home.locationServices,
                                  message: Localizations.Home.locationServicesMessage,
                                  preferredStyle: .alert,
                                  alertActions: actions)
                navigation?.present(alert)
                return
            }
            searchTweets(query: query)
        case .showTweetDetails(let tweet):
            navigation?.navigate(to: .showTweetDetails(tweet))
        case .radiusDidChange(let radius):
            state.radiusSearch = radius
            if let location = services.locationService.location?.coordinate {
                let radius = Double(state.radiusSearch)
                presenter?.apply(.showRadiusCircle(location: location, radius: radius))
                presenter?.apply(.centerMap(location: location, radius: radius))
            }
        case .configure(let view):
            guard let annotation = view.annotation as? TweetAnnotation else { return }
            configure(view: view, annotation: annotation)
        }
    }
}

extension HomeProcessor: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.state.showUserLocation = status == .authorizedWhenInUse
    }
}
