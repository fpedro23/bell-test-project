import MapKit

class TweetAnnotation: NSObject, MKAnnotation {

    let tweet: Tweet

    var coordinate: CLLocationCoordinate2D

    var title: String?

    var subtitle: String?

    init?(tweet: Tweet) {
        guard let position = tweet.coordinates, position.coordinates.count > 1  else { return nil }
        // Twitter's api returns the coordinate values in an array in format [lon,lat]. 🤔
        self.coordinate = CLLocationCoordinate2D(latitude: Double(position.coordinates[1]),
                                                     longitude: Double(position.coordinates[0]))
        self.tweet = tweet
        super.init()
    }
}
