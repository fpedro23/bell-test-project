import UIKit

enum HomeAction: Equatable {
    case radiusDidChange(radius: Int)
    case didTapUserLocation
    case showTweetDetails(Tweet)
    case searchTweets(String)
    case configure(TweetAnnotationView)
}

class HomeViewController: UIViewController {

    var dispatcher: Dispatcher<HomeAction>? {
        didSet {
            homeView.dispatcher = dispatcher
        }
    }

    lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = Localizations.Home.searchTweets
        return searchController
    }()

    let homeView = HomeView()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Tweets"
        self.view.backgroundColor = .systemBackground
        navigationItem.searchController = searchController
        view.addConstrained(subview: homeView, top: nil)
        homeView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
    }
}

extension HomeViewController: HomePresenter {
    func apply(_ effect: HomeEffect) {
        homeView.apply(effect)
    }

    func present(_ state: HomeState) {
        homeView.present(state)
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text, !text.isEmpty else { return }
        dispatcher?.dispatch(.searchTweets(text))
    }
}
