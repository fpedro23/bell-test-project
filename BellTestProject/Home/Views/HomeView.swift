import UIKit
import MapKit

class HomeView: UIView {

    var dispatcher: Dispatcher<HomeAction>? {
        didSet {
            radiusSelectionView.dispatcher = dispatcher
        }
    }

    lazy var locationButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "location"), for: .normal)
        button.backgroundColor = .systemBackground
        button.tintColor = .systemBlue
        button.contentEdgeInsets = UIEdgeInsets(top: 13, left: 13, bottom: 13, right: 13)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.adjustsImageSizeForAccessibilityContentSizeCategory = true
        button.addTarget(self, action: #selector(locationButtonTapped), for: .primaryActionTriggered)
        return button
    }()

    lazy var buttonContainer: UIStackView = {
        UIStackView(arrangedSubviews: [locationButton], axis: .vertical, spacing: 10)
    }()

    let mapView = MKMapView()

    var circleOverlay: MKCircle = MKCircle()

    let radiusSelectionView = RadiusSelectionView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .systemBackground
        mapView.delegate = self

        addConstrained(subview: mapView)
        addConstrained(subview: radiusSelectionView, top: 16, left: 16, bottom: nil, right: -16)
        addConstrained(subview: buttonContainer, top: nil, left: nil, bottom: -16, right: -16)

        NSLayoutConstraint.activate([
            locationButton.widthAnchor.constraint(greaterThanOrEqualToConstant: 44),
            locationButton.heightAnchor.constraint(greaterThanOrEqualTo: locationButton.widthAnchor)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        locationButton.layer.cornerRadius = locationButton.frame.width * 0.5
    }

    fileprivate func presentMapState(_ state: HomeState) {
        mapView.showsUserLocation = state.showUserLocation
        mapView.annotations.forEach { mapView.removeAnnotation($0) }
        mapView.addAnnotations(Array(state.mapAnnotations))
    }

    @objc func locationButtonTapped() {
        dispatcher?.dispatch(.didTapUserLocation)
    }
}

extension HomeView {
    func present(_ state: HomeState) {
        radiusSelectionView.present(state)
        presentMapState(state)
    }

    func apply(_ effect: HomeEffect) {
        switch effect {
        case .centerMap(let location, let radius):
            let initialLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
            let coordinateRegion = MKCoordinateRegion(center: initialLocation.coordinate,
                                                      latitudinalMeters: radius,
                                                      longitudinalMeters: radius * 2)
            mapView.setRegion(coordinateRegion, animated: true)
        case .showRadiusCircle(let location, let radius):
            mapView.removeOverlay(circleOverlay)
            circleOverlay = MKCircle(center: location, radius: radius)
            mapView.addOverlay(circleOverlay)
        }
    }
}

extension HomeView: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let view = view as? TweetAnnotationView else { return }
        dispatcher?.dispatch(.configure(view))
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKCircleRenderer(overlay: overlay)
        renderer.fillColor = UIColor.systemBlue.withAlphaComponent(0.5)
        renderer.strokeColor = UIColor.systemBlue.withAlphaComponent(0.8)
        return renderer
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? TweetAnnotation else { return nil }
        if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "TweetAnnotationView") as? TweetAnnotationView {
            annotationView.annotation = annotation
            annotationView.displayPriority = .required
            return annotationView
        } else {
            let annotationView = TweetAnnotationView(annotation: annotation, reuseIdentifier: "TweetAnnotationView")
            annotationView.dispatcher = dispatcher
            return annotationView
        }
    }
}
