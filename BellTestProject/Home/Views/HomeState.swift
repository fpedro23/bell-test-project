import Foundation

struct HomeState: Equatable {

    var showUserLocation: Bool = false

    private (set) var tweets: [Tweet] = []

    private var annotations: [TweetAnnotation] = []

    var mapAnnotations: [TweetAnnotation] {
        annotations.suffix(100)
    }

    var currentSearchQuery: String = ""

    var radiusSearch: Int = 5000

    var nextPage: String?

    mutating func didReceiveSearchResults(results: TweetSearchResults) {
        tweets.append(contentsOf: results.tweets)
        nextPage = results.next
        annotations.append(contentsOf: tweets.compactMap { TweetAnnotation(tweet: $0)})
    }

    mutating func searchQueryDidChange(newQuery: String) {
        currentSearchQuery = newQuery
        tweets = []
        annotations = []
    }
}

extension HomeState {
    var radiusSearchText: String {
        "\(radiusSearch/1000) km"
    }
}
