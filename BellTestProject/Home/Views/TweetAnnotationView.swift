import MapKit
import TwitterKit

class TweetAnnotationView: MKMarkerAnnotationView {

    let seeMoreButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizations.Home.showMore, for: .normal)
        button.heightAnchor.constraint(greaterThanOrEqualToConstant: 44).isActive = true
        button.addTarget(self, action: #selector(didTapSeeMoreButton), for: .primaryActionTriggered)
        button.setTitleColor(.systemBlue, for: .normal)
        return button
    }()

    lazy var tweetView: TWTRTweetView = TWTRTweetView(tweet: nil, style: .compact)

    lazy var stackView: UIStackView = {
        UIStackView(arrangedSubviews: [tweetView, seeMoreButton], axis: .vertical, spacing: 8)
    }()

    var dispatcher: Dispatcher<HomeAction>?

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        canShowCallout = true
        displayPriority = .required
        stackView.alignment = .center
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func didTapSeeMoreButton() {
        guard let annotation = annotation as? TweetAnnotation else { return }
        dispatcher?.dispatch(.showTweetDetails(annotation.tweet))
    }

    func configureView(tweet: TWTRTweet) {
        tweetView.configure(with: tweet)
        detailCalloutAccessoryView = stackView
    }
}
