import UIKit

class RadiusSelectionView: UIView {

    var dispatcher: Dispatcher<HomeAction>?

    lazy var slider: UISlider = {
        let slider = UISlider()
        slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        slider.minimumValue = 0
        slider.maximumValue = 40 * 1000
        slider.isContinuous = false
        return slider
    }()

    let label = UILabel()

    lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [slider, label], axis: .vertical)
        stackView.alignment = .center
        return stackView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addConstrained(subview: stackView, top: 8, left: 16, bottom: -8, right: -16)
        slider.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
        backgroundColor = .systemBackground
        layer.cornerRadius = 16
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func sliderValueChanged() {
        dispatcher?.dispatch(.radiusDidChange(radius: Int(slider.value)))
    }

    func present(_ state: HomeState) {
        slider.value = Float(state.radiusSearch)
        label.text = state.radiusSearchText
    }
}
