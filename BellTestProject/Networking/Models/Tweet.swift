import Foundation

struct Tweet: Equatable, Codable, Hashable {

    let identifier: Int64

    let text: String

    let user: User

    let coordinates: Coordinates?

    let place: Place?

    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case text
        case user
        case coordinates
        case place
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
}
