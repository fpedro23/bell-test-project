import Foundation

struct User: Equatable, Codable {
    //swiftlint:disable identifier_name
    let id: Int64
    let name: String
}
