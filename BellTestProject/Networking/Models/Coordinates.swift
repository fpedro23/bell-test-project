import Foundation

struct Coordinates: Equatable, Codable, Hashable {

    let coordinates: [Float]
}
