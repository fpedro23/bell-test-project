import Foundation

struct TweetSearchResults: Codable {
    let tweets: [Tweet]

    let next: String?

    enum CodingKeys: String, CodingKey {
        case tweets = "results"
        case next
    }
}
