import Foundation

struct Place: Equatable, Codable, Hashable {

    let identifier: String

    let boundingBox: BoundingBox

    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case boundingBox = "bounding_box"
    }
}
