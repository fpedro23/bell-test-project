import Foundation

struct BoundingBox: Equatable, Codable, Hashable {
    let coordinates: [[[Float]]]
}
