import Foundation
import CoreLocation

struct TweetSearchRequest {

    let base = "https://api.twitter.com/1.1"

    var path: String {
        base + "/tweets/search/fullarchive/dev.json"
    }

    let method = "GET"

    let searchQuery: String

    /// The radius of the search.
    /// The value of this search should be less than 40km due to twitter's limitation.
    /// https://developer.twitter.com/en/docs/tutorials/filtering-tweets-by-location
    let radius: Int

    let location: CLLocationCoordinate2D?

    var next: String?

    private var nextQuery: String? {
        nil
//        guard let next = next else { return nil }
//        return "next:\(next)"
    }

    private var pointRadiusQuery: String? {
        guard let location = location else { return nil }
        return "point_radius:[\(location.longitude) \(location.latitude) \(Int(radius/1000))km]"
    }

    var query: [AnyHashable: Any] {
        let query = [searchQuery, pointRadiusQuery, nextQuery]
            .compactMap { $0 }
            .joined(separator: " ")
        return ["query": query]
    }
}
