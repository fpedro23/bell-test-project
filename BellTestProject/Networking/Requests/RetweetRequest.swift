import Foundation
import CoreLocation

struct RetweetRequest {

    let base = "https://api.twitter.com/1.1"

    var path: String {
        base + "/statuses/retweet/\(String(tweetIdentifier)).json"
    }

    let method = "POST"

    let tweetIdentifier: Int64

    var query: [AnyHashable: Any] {
        ["id": String(tweetIdentifier)]
    }
}
