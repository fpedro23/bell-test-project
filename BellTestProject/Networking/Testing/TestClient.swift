import Foundation
import CoreLocation
import TwitterKit

class TestClient: TwitterAPI {

    var errorToReturn: Error?
    var loginProcessStarted = false

    func retweet(tweetIdentifier: Int64, userIdentifier: String, completion: @escaping (Result<Tweet, Error>) -> Void) -> Progress {
        if let error = errorToReturn {
            completion(.failure(error))
            return Progress()
        }
        do {
            let jsonDecoder = JSONDecoder()
            let data = try testData(for: .tweet)
            let result = Result { try jsonDecoder.decode(Tweet.self, from: data) }
            completion(result)
        } catch let error {
            completion(.failure(error))
        }
        return Progress()
    }

    func logIn(completion: @escaping (Result<TWTRSession, Error>) -> Void) {
        loginProcessStarted = true
        let session = TWTRSession(authToken: "🔥", authTokenSecret: "👀", userName: "⭐️", userID: "🐢")
        completion(.success(session))
    }

    func loadTweet(identifier: String, completion: @escaping (Result<TWTRTweet, Error>) -> Void) {
        if let error = errorToReturn {
            completion(.failure(error))
            return
        }
        do {
            let data = try testData(for: .tweet)
            let dictionary = try JSONSerialization.jsonObject(with: data) as? [AnyHashable: Any]
            completion(.success(TWTRTweet(jsonDictionary: dictionary!)!))
        } catch let error {
            completion(.failure(error))
        }
    }

    func searchTweets(query: String,
                      location: CLLocationCoordinate2D?,
                      radius: Int,
                      next: String?,
                      completion: @escaping (Result<TweetSearchResults, Error>) -> Void) -> Progress {
        if let error = errorToReturn {
            completion(.failure(error))
            return Progress()
        }
        do {
            let jsonDecoder = JSONDecoder()
            let data = try testData(for: .tweetSearchResults)
            let result = Result { try jsonDecoder.decode(TweetSearchResults.self, from: data) }
            completion(result)
        } catch let error {
            completion(.failure(error))
        }

        return Progress()
    }

    private func testData(for request: APITestData) throws -> Data {
        guard
            let url = Bundle.main.url(forResource: request.rawValue, withExtension: "json") else {
                throw TestError.testDataNotFound
        }

        return try Data(contentsOf: url)
    }
}

enum APITestData: String, CaseIterable {
    case tweetSearchResults
    case tweet
}

enum TestError: Error {
    case noResult
    case testDataNotFound
}
