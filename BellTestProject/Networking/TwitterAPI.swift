import Foundation
import TwitterKit
import CoreLocation

protocol TwitterAPI {

    func logIn(completion: @escaping (_ result: Result<TWTRSession, Error>) -> Void)

    @discardableResult func searchTweets(query: String,
                                         location: CLLocationCoordinate2D?,
                                         radius: Int,
                                         next: String?,
                                         completion: @escaping (_ result: Result<TweetSearchResults, Error>) -> Void) -> Progress

    func loadTweet(identifier: String,
                   completion: @escaping (_ result: Result<TWTRTweet, Error>) -> Void)

    @discardableResult func retweet(tweetIdentifier: Int64,
                                    userIdentifier: String,
                                    completion: @escaping (_ result: Result<Tweet, Error>) -> Void) -> Progress
}
