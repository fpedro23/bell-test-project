import TwitterKit

extension TWTRSessionStore: SessionStore {
    func didLoadSession(session: TWTRAuthSession) {
        //NO-OP Twitter's authentication stored is updated behind the scenes by the framework
    }

    var currentSession: TWTRAuthSession? {
        session()
    }
}
