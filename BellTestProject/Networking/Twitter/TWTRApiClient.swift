import CoreLocation
import TwitterKit

extension TWTRAPIClient: TwitterAPI {
    func retweet(tweetIdentifier: Int64, userIdentifier: String, completion: @escaping (Result<Tweet, Error>) -> Void) -> Progress {
        var clientError: NSError?
        let client = TWTRAPIClient(userID: userIdentifier)
        let request = RetweetRequest(tweetIdentifier: tweetIdentifier)
        let urlRequest = client.urlRequest(withMethod: request.method,
                                           urlString: request.path,
                                           parameters: request.query,
                                           error: &clientError)
        return client.sendTwitterRequest(urlRequest) { _, data, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            let jsonDecoder = JSONDecoder()
            let result = Result { try jsonDecoder.decode(Tweet.self, from: data ?? Data()) }
            completion(result)
        }
    }

    func logIn(completion: @escaping (Result<TWTRSession, Error>) -> Void) {
        TWTRTwitter.sharedInstance().logIn { (session, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let session = session else {
                completion(.failure(error!))
                return
            }
            completion(.success(session))
        }
    }

    func loadTweet(identifier: String, completion: @escaping (Result<TWTRTweet, Error>) -> Void) {
        loadTweet(withID: identifier) { tweet, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let tweet = tweet else {
                completion(.failure(error!))
                return
            }
            completion(.success(tweet))
        }
    }

    func searchTweets(query: String,
                      location: CLLocationCoordinate2D?,
                      radius: Int,
                      next: String?,
                      completion: @escaping (Result<TweetSearchResults, Error>) -> Void) -> Progress {
        let request = TweetSearchRequest(searchQuery: query, radius: radius, location: location, next: next)
        var clientError: NSError?
        let urlRequest = self.urlRequest(withMethod: request.method,
                                         urlString: request.path,
                                         parameters: request.query,
                                         error: &clientError)
        return sendTwitterRequest(urlRequest) { _, data, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            let jsonDecoder = JSONDecoder()
            let result = Result { try jsonDecoder.decode(TweetSearchResults.self, from: data ?? Data()) }
            completion(result)
        }
    }

    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}
