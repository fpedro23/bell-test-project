import Foundation
import TwitterKit

protocol SessionStore {
    var currentSession: TWTRAuthSession? { get }

    func didLoadSession(session: TWTRAuthSession)
}
