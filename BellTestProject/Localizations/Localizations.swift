import Foundation

enum Localizations {
    enum Home {

        static let locationServices = NSLocalizedString(
            "Home.LocationServices",
            value: "Location services",
            comment: "The value for the title of the location services alert."
        )

        static let locationServicesMessage = NSLocalizedString(
            "Home.LocationServicesMessage",
            value: "Location services are needed for this app to function, enable location services and try again",
            comment: "The message for the location services aler"
        )

        static let authorize = NSLocalizedString(
            "Home.Authorize",
            value: "Authorize",
            comment: "The value for the authorize button."
        )

        static let accept = NSLocalizedString(
            "Home.Ok",
            value: "Ok",
            comment: "The value for OK button."
        )

        static let cancel = NSLocalizedString(
            "Home.Cancel",
            value: "Cancel",
            comment: "The value for cancel button."
        )

        static let searchTweets = NSLocalizedString(
            "Home.SearchTweets",
            value: "Search tweets",
            comment: "The search tweets text."
        )

        static let showMore = NSLocalizedString(
            "Home.ShowMore",
            value: "Show more",
            comment: "The show more button text."
        )
    }
    enum TweetDetails {
        static let success = NSLocalizedString(
            "TweetDetails.Success",
            value: "Success",
            comment: "The success text"
        )

        static let retweetSuccessMessage = NSLocalizedString(
            "TweetDetails.RetweetSuccessMessage",
            value: "The tweet has been retweeted succesfully.",
            comment: "The retweet success text"
        )
    }
    enum Error {
        static let title = NSLocalizedString(
            "Error.Title",
            value: "Error",
            comment: "Error title"
        )
    }
}
